class AddTagging < ActiveRecord::Migration
  def change
    create_table :taggings do |t|
      t.integer :tag_topic_id
      t.integer :shortened_url_id
      t.integer :tagger_id

      t.timestamps
    end
  end
end
