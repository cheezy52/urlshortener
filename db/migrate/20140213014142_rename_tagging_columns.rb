class RenameTaggingColumns < ActiveRecord::Migration
  def change
    rename_column(:taggings, :shortened_url, :shortened_url_id)
    rename_column(:taggings, :tag_topic, :tag_topic_id)
  end
end
