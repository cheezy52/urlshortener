# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
URLShortener::Application.config.secret_key_base = 'a33b87d0d6d42ebb4036f024639ba8efc8cd27e4afa45f361a5ffdf83819dd7688bad1eff5af200320e680b23a23bef9f5c5a874a5efcee0b9f8c515ea8ccaac'
