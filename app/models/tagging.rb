class Tagging < ActiveRecord::Base

  validates :shortened_url_id, :tag_topic_id, :presence => true
  validate :not_already_applied

  belongs_to(
    :shortened_url,
    :primary_key => :id,
    :foreign_key => :shortened_url_id,
    :class_name => "ShortenedURL"
  )

  belongs_to(
    :tag_topic,
    :primary_key => :id,
    :foreign_key => :tag_topic_id,
    :class_name => "TagTopic"
  )

  def not_already_applied
    if !Tagging.all.
      where("tag_topic_id = ? AND shortened_url_id = ?",
        :tag_topic_id, :shortened_url_id).empty?
      errors[:base] << "URL is already tagged with this topic."
    end
  end
end