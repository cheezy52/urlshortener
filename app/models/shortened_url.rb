class ShortenedURL < ActiveRecord::Base

  validates :long_url, :short_url, :submitter_id, :presence => true
  validates :short_url, :uniqueness => true


  belongs_to(
    :submitter,
    :primary_key => :id,
    :foreign_key => :submitter_id,
    :class_name => "User"
  )

  has_many(
    :visits,
    :primary_key => :id,
    :foreign_key => :shortened_url_id,
    :class_name => "Visit"
  )

  has_many(
    :visitors, -> { uniq },
    :through => :visits,
    :source => :user
  )

  has_many(
    :taggings,
    :primary_key => :id,
    :foreign_key => :shortened_url_id,
    :class_name => "Tagging"
  )

  has_many(
    :tag_topics,
    :through => :taggings,
    :source => :tag_topic
  )

  def self.random_code
    short_url = SecureRandom::urlsafe_base64(16)
    while self.all.map { |shortened_url| shortened_url.short_url }.include?(short_url)
      short_url = SecureRandom::urlsafe_base64(16)
    end
    short_url
  end

  def self.create_for_user_and_long_url!(user, long_url)
    short_url = ShortenedURL.random_code
    ShortenedURL.new(
      :long_url => long_url,
      :short_url => short_url,
      :submitter_id => user.id)
  end

  def num_clicks
    self.visits.count
  end

  def num_uniques
    self.visitors.count
  end

  def num_recent_uniques
    self.visits
      .where("created_at > ?", 10.minutes.ago)
      .select(:user_id)
      .distinct
      .count
  end

  def self.most_popular(n)
    #Not working; move to Visit to avoid "all" problem maybe?
    self.all
    .visits
    .select("count(id) AS click_count")
    .group(:shortened_url_id)
    .order(:click_count, :desc)
    .limit(n)
  end
end