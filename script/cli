require 'launchy'

def get_user
  user = nil
  begin
    puts "What is your email?"
    input = gets.chomp
    user = User.find_by_email(input)
    raise "That is not a valid email address." if user.nil?
  rescue => e
    puts e
    retry
  end
  user
end

def select_action(user)
  begin
    puts "Would you like to visit (v) or create (c) a shortened URL?"
    input = gets.chomp.downcase
    case input
    when "v"
      visit(user)
    when "c"
      create(user)
    else
      raise "Invalid option.  Please select (v)isit or (c)reate."
    end
  rescue => e
    puts e
    retry
  end
  nil
end

def create(user)
  long_url = nil
  begin
    puts "Please input the long URL you wish to shorten."
    input = gets.chomp
    raise "Please enter a URL." if input.empty?
    long_url = input
  rescue => e
    puts e
    retry
  end

  shortened_url = ShortenedURL.create_for_user_and_long_url!(user, long_url)
  shortened_url.save!
  puts "Your shortened URL is #{shortened_url.short_url}"
  launch(user, shortened_url)
end

def visit(user)
  shortened_url = nil
  begin
    puts "Please input the shortened URL you wish to visit."
    input = gets.chomp
    raise "Please enter a URL." if input.empty?
    short_url = input
    shortened_url = ShortenedURL.find_by_short_url(short_url)
    raise "That is not a valid shortened URL." if shortened_url.nil?
  rescue => e
    puts e
    retry
  end
  launch(user, shortened_url)
end

def launch(user, shortened_url)
  puts "Opening your URL now.  Enjoy!"
  Launchy.open(shortened_url.long_url)
  Visit.record_visit!(user, shortened_url).save!
end

if __FILE__ == $PROGRAM_NAME
  user = get_user
  select_action(user)
end